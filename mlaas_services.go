//go:generate protoc mlaas_services.proto --go_out=plugins=grpc:.

package mlaasservices

var (
	DbServiceDescription     = _DbService_serviceDesc
	NNServiceDescription     = _NNService_serviceDesc
	ServerServiceDescription = _ServerService_serviceDesc
)
